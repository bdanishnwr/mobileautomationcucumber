**Framework Overview**

-Its BDD Framework 
-Framework is devoloped to support iOS and Android testing both using same test Feature file 
-Tool and Technology Used 
   -Cucumber 
   -Spring 
   -Appium (Java)
   -Spark Extent For Test Report

---

**Test Environment Setup**

	Setup Appium Server 
	Setup an Emulator iOS/Android or Connect Real device into Machine where Appium running 
	Install Maven 
	
	
**Test Execution Parameter**

    platform - ios/Android Default Android :- This is to select on which device Script to run 
	app.path - This is for passing Andoid APP path or iOS App Path
	appium.host - Appium Host Url Default Path is  "http://localhost:4723/wd/hub" 
	device.deviceName - Targetting Device Name to be provided 
	
**Execution Command ** 

 mvn clean test -DPlatform=Android -Dapp.path=<AppParth> -Dappium.host=http://localhost:4723/wd/hub  -Ddevice.deviceName=Pixel_5
 
**Report** 

Report will be generated inside folder name test-output along with screenshot 
	
	
	
	
	