package com.assignment.config;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.io.File;

@Configuration
@PropertySource({"file:src/main/resources/${platform:Android}/driver.properties",
            "file:src/main/resources/${platform:Android}/xpath.properties"})
public class ConfigData {


    @Value("${app.path}")
    private String appPathV;

    @Value("${device.platformName:Android}")
    private String platformName;

    @Bean
    public DesiredCapabilities getDesiredCapabilities(){
        File classpathRoot = new File(System.getProperty("user.dir"));
        File appPath = new File(classpathRoot,appPathV);

        DesiredCapabilities capabilities = new DesiredCapabilities();
        //General Capabilities
        capabilities.setCapability("platformName", platformName);
        capabilities.setCapability("app", appPath.getAbsolutePath());


        //Android Capabilities Need to move under condition
        capabilities.setCapability("appPackage", "com.swaglabsmobileapp");
        capabilities.setCapability("appActivity", ".SplashActivity");
        return capabilities;

    }


}
