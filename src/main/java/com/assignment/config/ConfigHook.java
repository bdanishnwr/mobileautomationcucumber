package com.assignment.config;

import com.assignment.SpringContextConfiguration;
import com.assignment.util.AppDriver;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.spring.CucumberContextConfiguration;
import org.openqa.selenium.OutputType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.MalformedURLException;

@CucumberContextConfiguration
@SpringBootTest(classes = {SpringContextConfiguration.class})
public class ConfigHook {

    @Autowired
    private AppDriver appDriver;

    @Before
    public void beforeTest() throws MalformedURLException {
        appDriver.setDriver();
    }

    @After(order = 0)
    public void afterTest() {
        if(appDriver.getDriver()!=null)
            appDriver.getDriver().closeApp();
    }


    @After(order = 1)
    public void afterTestTakeScreenShot(Scenario scenario) {
        if (scenario.isFailed() && appDriver.getDriver()!=null) {
            byte[] sourcePath = appDriver.getDriver().getScreenshotAs(OutputType.BYTES);
            scenario.attach(sourcePath, "image/png", "FailedTest");
        }
    }
}
