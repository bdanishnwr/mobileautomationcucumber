package com.assignment.config;


import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ContextData {

    private ThreadLocal<Map<String,Object>>  contextData = new ThreadLocal<Map<String, Object>>();

    private Map<String,Object> getContext() {
        if (contextData.get() == null)
            contextData.set(new HashMap<String, Object>());
        return contextData.get();
    }

    public void setContextValue(String key, Object value) {
        getContext().put(key,value);
    }

    public <T> T getContextValueByType(String key, Class <T> classType){
      return classType.cast(getContext().get(key));
    }


    public String getContextValueAsString(String key){
        return getContext().get(key).toString();
    }

}
