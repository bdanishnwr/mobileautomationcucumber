package com.assignment.util;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class AppDriver {


    @Autowired
    private DesiredCapabilities desiredCapabilities;

    @Value("${appium.host}")
    private String appiumHostUrl;

    private ThreadLocal<AppiumDriver> androidDriverS = new ThreadLocal<AppiumDriver>();

    public void setDriver() throws MalformedURLException {
        if (androidDriverS.get() == null) {
            AppiumDriver androidDriver = new AndroidDriver(new URL(appiumHostUrl), desiredCapabilities);
            androidDriverS.set(androidDriver);
        }
    }

    public AppiumDriver getDriver() {
        return androidDriverS.get();
    }
}
