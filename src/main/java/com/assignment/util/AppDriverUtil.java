package com.assignment.util;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AppDriverUtil {

    @Autowired
    private AppDriver appDriver;

    @Value("${waitTime:30}")
    private long waitTime;

    private AppiumDriver getAppiumDriver(){
        return appDriver.getDriver();
    }

    private WebDriverWait getWebDriverWait(){
        return getWebDriverWait(waitTime);
    }

    private WebDriverWait getWebDriverWait(long cWaitTime){
        return new WebDriverWait(getAppiumDriver(),cWaitTime);
    }

    public WebElement findByXpath(String xpath){
       return getAppiumDriver().findElement(By.xpath(xpath));
    }

    public boolean waitForElementToAppear(String xPath) {
        WebDriverWait webDriverWait = getWebDriverWait();
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
            return true;
        } catch (Exception ex) {
            System.out.println("Element not loaded");
        }
        return false;
    }

    public boolean waitForElementTextToAppear(String text,long waitTime) {
        WebDriverWait webDriverWait = getWebDriverWait(waitTime);
        String xPath = String.format("//*[@text='%s']",text);
        try {
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
            return true;
        } catch (Exception ex) {
            System.out.println("Element not loaded");
        }
        return false;
    }

    public boolean waitForElementTextToAppear(String text) {
        return waitForElementTextToAppear(text,waitTime);
    }

    public WebElement findById(String xpath){
        return getAppiumDriver().findElement(By.id(xpath));
    }

    public WebElement findByText(String xpath){
        return getAppiumDriver().findElement(By.tagName(xpath));
    }

    public void performDragAndDrop(String fromXpath, String toXpath){
        WebElement fromEle = findByXpath(fromXpath);
        WebElement toEle = findByXpath(toXpath);
        TouchActions touchActions = new TouchActions(getAppiumDriver());
        touchActions.longPress(fromEle).moveToElement(toEle).perform();
    }


    public List<WebElement> findElementListByXpath(String productItemList) {
        return getAppiumDriver().findElements(By.xpath(productItemList));
    }
}
