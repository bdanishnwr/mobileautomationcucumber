package com.assignment.steps;

import com.assignment.view.LoginView;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;


public class LoginStep {


    @Autowired
    private LoginView loginView;


    @When("^I Enter the username with \"(.*)\"$")
    public void enterLogin(String userName) {
        loginView.enterLoinId(userName);
    }


    @When("^I Enter the password with \"(.*)\"$")
    public void enterPassword(String password) {
        loginView.enterLoinPassword(password);
    }

    @When("^I click on Login button$")
    public void clickLoginButton() {
        loginView.clickLoginButton();
    }

    @Then("I expect error message \"(.*)\"$")
    public void validateErrorMessage(String errorMessage) {
        Assertions.assertThat(loginView.getErrorMessage()).isEqualTo(errorMessage);
    }

    @Then("I expect error alert message to appear")
    public void validateErrorMessageShown() {
        Assertions.assertThat(loginView.isErrorMessage()).isEqualTo(true);
    }


}
