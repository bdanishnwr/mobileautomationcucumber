package com.assignment.steps;


import com.assignment.config.ContextData;
import com.assignment.util.AppDriver;
import com.assignment.view.CommonView;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonStep {

    @Autowired
    private CommonView commonView;

    @Autowired
    private AppDriver appDriver;

    @Autowired
    private ContextData contextData;


    @Given("Re Open Application")
    public void openApplication(){
        appDriver.getDriver().launchApp();
    }



    @Given("Wait for Application Login Page to launch")
    public void waitForLoginPage(){
        commonView.waitForElementToAppear("The currently accepted usernames for this application are (tap to autofill):");
    }

    @Then("Alert message appeared")
    public void alertMessageAppeared(){
        commonView.waitForElementToAppear("Username and password do not match any user in this service.");
    }


}
