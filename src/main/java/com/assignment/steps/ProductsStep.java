package com.assignment.steps;

import com.assignment.view.ProductsView;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductsStep {

    @Autowired
    private ProductsView productsView;

    @When("User Add first product to cart from List")
    public void selectFirstItemInList(){
        productsView.selectFirstItem();
    }

    @When("User Remove first product to cart from List")
    public void removeFirstItemInList(){
        productsView.removeProductFromCart();
    }

    @Given("Wait for Product view to Open")
    public void waitForProductViewToOpen(){
        productsView.waitForProductView();
    }

    @Given("Save first product price from List and save in context var \"(.*)\"$")
    public void selectFirstItemPrice(String conVar){
        productsView.saveFirstProductPrice(conVar);
    }

    @Given("Save first product title from List and save in context var \"(.*)\"$")
    public void saveFirstProductTitle(String conVar){
        productsView.saveFirstProductTitle(conVar);
    }


    @Then("Cart have one product")
    public void cartHaveOneProduct(){
        productsView.cartHaveOneProduct();
    }

    @Then("Cart have no product")
    public void cartHaveNoProduct(){
        productsView.cartHaveNoProduct();
    }




}
