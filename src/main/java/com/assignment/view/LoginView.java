package com.assignment.view;

import com.assignment.util.AppDriverUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class LoginView {


    @Autowired
    private AppDriverUtil appDriverUtil;

    @Value("${login.btn}")
    private String loginBtn;

    @Value("${login.id}")
    private String loginId;

    @Value("${login.password}")
    private String loginPassword;

    @Value("${login.errorMessage}")
    private String errorMessage;

    @Value("${login.view}")
    private String loginView;


    public void enterLoinId(String id){
        appDriverUtil.findByXpath(loginId).sendKeys(id);
    }

    public void enterLoinPassword(String id){
        appDriverUtil.findByXpath(loginPassword).sendKeys(id);
    }

    public void clickLoginButton(){
        appDriverUtil.findByXpath(loginBtn).click();
    }

    public boolean isErrorMessage() {
        return appDriverUtil.waitForElementToAppear(errorMessage);
    }

    public String getErrorMessage() {
       return appDriverUtil.findByXpath(errorMessage).getText();
    }
}
