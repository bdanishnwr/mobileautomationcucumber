package com.assignment.view;

import com.assignment.util.AppDriverUtil;
import org.assertj.core.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommonView {

    @Autowired
    private AppDriverUtil appDriverUtil;

    public void waitForElementToAppear(String text) {
        Assertions.assertThat(appDriverUtil.waitForElementTextToAppear(text));
    }
}
