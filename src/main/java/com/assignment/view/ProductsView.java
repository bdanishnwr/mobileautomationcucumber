package com.assignment.view;

import com.assignment.config.ContextData;
import com.assignment.util.AppDriverUtil;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductsView {

    @Autowired
    private AppDriverUtil appDriverUtil;

    @Autowired
    private ContextData contextData;

    @Value("${product-cartItem}")
    private String productInCart;

    @Value("${product-itemList}")
    private String productItemList;

    @Value("${product-price}")
    private String productPrice;

    @Value("${product-title}")
    private String productTitle;

    @Value("${product-addToCart}")
    private String productAddToCart;

    @Value("${product-remove}")
    private String productRemove;




    private  WebElement addProductEle(WebElement parentEle){
        return   parentEle.findElement(By.xpath(productAddToCart));
    }

    private  WebElement removeProductEle(WebElement parentEle){
        return   parentEle.findElement(By.xpath(productRemove));
    }

    private WebElement getProductPriceEle(WebElement parentEle){
       return parentEle.findElement(By.xpath(productPrice));
    }

    private WebElement getProductTitleEle(WebElement parentEle){
        return parentEle.findElement(By.xpath(productTitle));
    }

    private List<WebElement> getProductListEle(){
        return appDriverUtil.findElementListByXpath(productItemList);
    }


    public void saveFirstProductPrice(String conVar) {
       WebElement webElement = getProductListEle().get(0);
       String prodPrice = getProductPriceEle(webElement).getText();
       contextData.setContextValue(conVar,prodPrice);
    }

    public void saveFirstProductTitle(String conVar) {
        WebElement webElement = getProductListEle().get(0);
        String prodTitle = getProductTitleEle(webElement).getText();
        contextData.setContextValue(conVar,prodTitle);
    }

    public void selectFirstItem() {
        WebElement webElement = getProductListEle().get(0);
        addProductEle(webElement).click();
    }

    public void removeProductFromCart() {
        WebElement webElement = getProductListEle().get(0);
        removeProductEle(webElement).click();
    }

    public void waitForProductView() {
        Assertions.assertThat(appDriverUtil.waitForElementToAppear(productInCart)).isEqualTo(true);
    }

    public void cartHaveOneProduct() {
       Assertions.assertThat(appDriverUtil.findByXpath(productInCart).findElement(By.xpath("//android.widget.TextView")).getText()).isEqualTo("1");
    }

    public void cartHaveNoProduct() {
        Assertions.assertThat(appDriverUtil.findByXpath(productInCart).findElements(By.xpath("//android.widget.TextView")).size()).isEqualTo(0);
    }
}
