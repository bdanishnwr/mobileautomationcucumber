
Feature: Login To Application

  Scenario: Verification Of Login with Invalid Credential
    Given Wait for Application Login Page to launch
    When I Enter the username with "testUser"
    And I Enter the password with "testPassword"
    And I click on Login button
    Then I expect error alert message to appear
    And I expect error message "Username and password do not match any user in this service."


  Scenario: Verification Of Login with Valid Credential
    Given Re Open Application
    And Wait for Application Login Page to launch
    When I Enter the username with "standard_user"
    And I Enter the password with "secret_sauce"
    And I click on Login button
    Then I expect error alert message to appear
    And I expect error message "Username and password do not match any user in this service."