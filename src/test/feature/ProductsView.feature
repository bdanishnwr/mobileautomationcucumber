
Feature: Verify Adding Item

  Scenario: Login to Application
    Given Re Open Application
    And Wait for Application Login Page to launch
    When I Enter the username with "standard_user"
    And I Enter the password with "secret_sauce"
    And I click on Login button

    Given Wait for Product view to Open
    And Save first product price from List and save in context var "productPrice"
    And Save first product title from List and save in context var "productTitle"
    When User Add first product to cart from List
    Then Cart have one product
    When User Remove first product to cart from List
    Then Cart have no product
    And Cart have one product




